
<script type="x-template" id="app-template">
    <div>
        <app-nav></app-nav>
        <div class="container">
            <router-view class="view"></router-view>
        </div>
    </div>
</script>

<script type="x-template" id="progress-bar-template">
    <div>
        <div class="progress">
            <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
        </div>
        <h3 style="text-align: center">{{ message }}</h3>
    </div>
</script>

<script type="x-template" id="navbar-template">
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#/">Tiny Library</a>
            </div>
            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <ul class="nav navbar-nav">
                    <li><a href="#/catalog">Catalogs</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a target="_blank">
                            <span>User</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</script>

<script type="x-template" id="catalogs-template">
    <div>
        <div class="list-group" v-if="catalogs.length">
            <router-link v-for="(catalog, index) in catalogs" :to="{ name: 'books', params: { catalogId: catalog.id }}" class="list-group-item">
                <span>{{ index + 1 }}. {{ catalog.name }}</span>
            </router-link>
        </div>
        <div v-else>
            <progress-bar message="Catalog is loading..."></progress-bar>
        </div>
    </div>
</script>

<script type="x-template" id="books-template">
    <div>
        <h3>{{ catalog.name }}</h3>
        <div class="text-right" style="margin: 10px 0">
            <router-link :to="{ name: 'newBook', params: { catalogId: $route.params.catalogId }}" class="btn btn-primary">
                Add new book
            </router-link>
        </div>
        <div v-if="catalog.books.length">
            <books-grid :books="catalog.books" @remove="onRemove"></books-grid>
        </div>
        <div v-else>
            <h3 v-if="noBooks">No books. Please, add one more book to catalog.</h3>
            <progress-bar message="Books is loading..." v-else></progress-bar>
        </div>
    </div>
</script>

<script type="x-template" id="books-grid-template">
    <table class="table">
        <thead>
        <tr>
            <th>#</th>
            <th>Name</th>
            <th>Author</th>
            <th>Publishing</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        <tr v-for="(item, index) in books">
            <td>{{ index + 1 }}</td>
            <td>{{ item.name }}</td>
            <td>{{ item.authors }}</td>
            <td>{{ item.publishDate }}</td>
            <td>
                <router-link :to="{ name: 'editBook', params: { catalogId: $route.params.catalogId, bookId: item.id }}"
                             class="btn btn-info btn-xs">
                    Edit
                </router-link>
                <router-link :to="{ name: 'moveBook', params: { catalogId: $route.params.catalogId, bookId: item.id }}"
                             class="btn btn-success btn-xs">
                    Move
                </router-link>
                <button class="btn btn-danger btn-xs" @click="onRemoveBook(item)">Remove</button>
            </td>
        </tr>
        </tbody>
    </table>
</script>

<script type="x-template" id="book-editor-template">
    <div>
        <div class="text-right" style="margin: 10px 0">
            <router-link :to="{ name: 'books', params: { catalogId: $route.params.catalogId }}"
                         class="btn btn-default">
                &lt; Back To Catalog
            </router-link>
        </div>
        <form class="form-horizontal" v-on:submit.prevent="onSubmit">
            <h3>Book Editor</h3>
            <div class="form-group">
                <label for="book-name" class="col-sm-4 control-label">Name</label>
                <div class="col-sm-8">
                    <input type="text" v-model="book.name" required class="form-control" id="book-name" placeholder="Name">
                </div>
            </div>
            <div class="form-group">
                <label for="book-authors" class="col-sm-4 control-label">Authors</label>
                <div class="col-sm-8">
                    <input type="text" v-model="book.authors" required class="form-control" id="book-authors" placeholder="Authors">
                </div>
            </div>
            <div class="form-group">
                <label for="book-publishing" class="col-sm-4 control-label">Publish</label>
                <div class="col-sm-8">
                    <date-picker :model="book.publishDate" @input="book.publishDate = arguments[0]" id="book-publishing"></date-picker>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                    <button type="submit" class="btn btn-info btn-default">Save</button>
                </div>
            </div>
        </form>
    </div>
</script>

<script type="x-template" id="book-move-template">
    <div>
        <div class="text-right" style="margin: 10px 0">
            <router-link :to="{ name: 'books', params: { catalogId: $route.params.catalogId }}"
                         class="btn btn-default">
                &lt; Back To Catalog
            </router-link>
        </div>
        <form class="form-horizontal" v-on:submit.prevent="onSubmit">
            <h3>Move Book to Catalog</h3>
            <p>Name: {{ book.name }}. Authors: {{ book.authors }}. Publish Date: {{ book.publishDate }}</p>
            <div class="form-group">
                <label for="book-catalog" class="col-sm-6 control-label">Belongs to Catalog</label>
                <div class="col-sm-6">
                    <select v-model="book.catalogId" class="form-control">
                        <option v-for="c in catalog" v-bind:value="c.id" id="book-catalog">
                            {{ c.name }}
                        </option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-6 col-sm-6">
                    <button type="submit" class="btn btn-info btn-default">Save</button>
                </div>
            </div>
        </form>
    </div>
</script>
