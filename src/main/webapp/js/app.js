(function ($, Vue, window) {
    var resources = (function services () {
        return {
            catalog: Vue.resource('api/catalog{/id}'),
            book: Vue.resource('api/book{/id}')
        };
    })();

    Vue.component('date-picker', {
        template: '<input type="text" required class="form-control" v-model="model" placeholder="Select Date">',
        props: ['model'],
        mounted: function () {
            var _this = this;
            $(this.$el).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true
            })
            .on('changeDate', function () {
                _this.model = $(_this.$el).val();
                _this.$emit("input", _this.model);
            });
        }
    });

    Vue.component('progress-bar', {
        template: '#progress-bar-template',
        props: ['message']
    });

    Vue.component('app-nav', {
        template: '#navbar-template'
    });

    Vue.component('books-grid', {
        props: ['books'],
        template: '#books-grid-template',
        methods: {
            onRemoveBook: function (book) {
                this.$emit("remove", book);
            }
        }
    });

    var IndexCatalogComponent =  Vue.component('index-catalog', {
        template: '#catalogs-template',
        created: function () {
            this.fetchData();
        },
        methods: {
            fetchData: function () {
                var _this = this;
                resources.catalog.query().then(function (r) {
                    _this.catalogs = r.body || [];
                });
            }
        },
        data: function () {
            return {
                catalogs: []
            }
        }
    });

    var IndexBookComponent = Vue.component('index-book', {
        template: '#books-template',
        created: function () {
            this.fetchData();
        },
        methods: {
            onBooksUpdate: function () {
                this.noBooks = this.catalog.books && !this.catalog.books.length;
            },
            fetchData: function () {
                var _this = this;
                resources.catalog.get({ id: this.$route.params.catalogId }).then(function (r) {
                    _this.catalog = r.body;
                    _this.onBooksUpdate();
                });
            },
            onRemove: function (book) {
                var _this = this;
                if(confirm("Remove book?")) {
                    resources.book.delete({id: book.id}).then(function (r) {
                        _this.catalog.books.splice(_this.catalog.books.indexOf(book), 1);
                        _this.onBooksUpdate();
                    });
                }
            }
        },
        data: function () {
            return {
                catalog: { books: [] },
                noBooks: false
            }
        }

    });

    var EditBookComponent = Vue.component('edit-book', {
        template: '#book-editor-template',
        created: function () {
            if (this.$route.params.bookId) {
                this.fetchData(this.$route.params.bookId);
            }
        },
        methods: {
            onSubmit: function () {
                var _this = this;
                if (!this.book.id) {
                    this.book.catalogId = this.$route.params.catalogId;
                }
                resources.book[this.book.id ? 'update' : 'save']({id: this.book.id}, this.book).then(function (r) {
                    _this.$router.push({ name: 'books', params: {
                        catalogId: _this.$route.params.catalogId
                    }});
                });
            },
            fetchData: function (bookId) {
                var _this = this;
                resources.book.get({ id: this.$route.params.bookId }).then(function (r) {
                    _this.book = r.body;
                });
            }
        },
        data: function () {
            return {
                book: {}
            }
        }
    });


    var MoveBookComponent = Vue.component('move-book', {
        template: '#book-move-template',
        created: function () {
            if (this.$route.params.bookId) {
                this.fetchData(this.$route.params.bookId);
            }
        },
        methods: {
            onSubmit: function () {
                var _this = this;
                resources.book.update({id: this.book.id}, this.book).then(function (r) {
                    _this.$router.push({ name: 'books', params: {
                        catalogId: _this.$route.params.catalogId
                    }});
                });
            },
            fetchData: function (bookId) {
                var _this = this;
                resources.catalog.query().then(function (r) {
                    _this.catalog = r.body;
                    return resources.book.get({id: _this.$route.params.bookId});
                }).then(function (r) {
                    _this.book = r.body;
                });
            }
        },
        data: function () {
            return {
                book: {},
                catalog: []
            }
        }
    });

    var router = new VueRouter({
        routes: [
            { path: '/catalog', component: IndexCatalogComponent, name: 'catalogs' },
            { path: '/catalog/:catalogId/books', component: IndexBookComponent, name: 'books' },
            { path: '/catalog/:catalogId/books/new', component: EditBookComponent, name: 'newBook' },
            { path: '/catalog/:catalogId/books/:bookId', component: EditBookComponent, name: 'editBook' },
            { path: '/catalog/:catalogId/books/:bookId/move', component: MoveBookComponent, name: 'moveBook' },

            { path: '*', redirect: '/catalog' }
        ]
    });

    var app = new Vue({
        router: router,
        el: '#app',
        template: '#app-template'
    });

    console.log('Ready.');
})(window.jQuery, window.Vue, window);