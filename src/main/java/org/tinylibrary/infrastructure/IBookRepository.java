package org.tinylibrary.infrastructure;

import org.tinylibrary.domain.model.Book;

import javax.ejb.Local;
import java.util.Collection;
import java.util.UUID;

@Local
public interface IBookRepository {
    Collection<Book> getAllByCatalogId(UUID catalogId);
    Book getById(UUID bookId);
    Book save(Book book);
    Book remove(Book book);
}
