package org.tinylibrary.infrastructure;

import org.tinylibrary.domain.model.Book;

import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.enterprise.inject.Default;
import java.util.*;

@Singleton
@Lock(LockType.WRITE)
@Default
public class BookInMemoryRepository implements IBookRepository {

    private HashMap<UUID, Book> collection;

    public BookInMemoryRepository() {
        initDefaults();
    }

    private void initDefaults() {
        collection = new HashMap<>();

        Book book;
        UUID catalog1 = UUID.fromString("9cdda1bf-fa8b-4f5f-b0c1-11794fa20717"),
            catalog2 = UUID.fromString("eca6cba5-5d9b-4947-833d-bf7add259b7e");

        book = new Book(catalog1, Arrays.asList("Author 1a", "Author 2a"), "Book A", new Date());
        book.setId(UUID.fromString("eca6cba5-5d9b-4947-833d-bf7add259b71"));
        collection.put(book.getId(), book);

        book = new Book(catalog1, Arrays.asList("Author 1b", "Author 2b"), "Book B", new Date());
        book.setId(UUID.fromString("eca6cba5-5d9b-4947-833d-bf7add259b72"));
        collection.put(book.getId(), book);


        book = new Book(catalog2, Arrays.asList("Author 1c", "Author 2c"), "Book C", new Date());
        book.setId(UUID.fromString("eca6cba5-5d9b-4947-833d-bf7add259b73"));
        collection.put(book.getId(), book);

        book = new Book(catalog2, Arrays.asList("Author 1d", "Author 2d"), "Book D", new Date());
        book.setId(UUID.fromString("eca6cba5-5d9b-4947-833d-bf7add259b74"));
        collection.put(book.getId(), book);
    }

    @Override
    public Collection<Book> getAllByCatalogId(UUID catalogId) {
        ArrayList<Book> result = new ArrayList<>();
        for(Book book : collection.values()) {
            if (book.getCatalogId() != null && book.getCatalogId().equals(catalogId)) {
                result.add(book);
            }
        }

        Collections.sort(result, new Comparator<Book>() {
            @Override
            public int compare(Book o1, Book o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });

        return result;
    }

    @Override
    public Book getById(UUID bookId) {
        if (collection.containsKey(bookId)) {
            return collection.get(bookId);
        }
        return null;
    }

    @Override
    synchronized public Book save(Book book) {
        if (book.getId() == null) {
            book.setId(UUID.randomUUID());
        }
        collection.put(book.getId(), book);
        return null;
    }

    @Override
    synchronized public Book remove(Book book) {
        if (collection.containsKey(book.getId())) {
            collection.remove(book.getId());
            return book;
        }
        return null;
    }

    //    @Override
//    public Collection<Book> getAll() {
//        return Collections.unmodifiableCollection(new ArrayList<>(collection));
//        return Collections.sort(collection, new Comparator<Book>() {
//            @Override
//            public int compare(Book o1, Book o2) {
//                return o1.getFirstName().compareTo(o2.getStartDate());
//            }
//        });
//    }

}
