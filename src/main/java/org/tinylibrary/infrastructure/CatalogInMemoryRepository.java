package org.tinylibrary.infrastructure;

import org.tinylibrary.domain.model.Catalog;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.enterprise.inject.Default;
import java.util.*;

@Singleton
@Lock(LockType.WRITE)
@Default
public class CatalogInMemoryRepository implements ICatalogRepository {

    private Map<UUID, Catalog> collection;

    public CatalogInMemoryRepository() {
        initDefaults();
    }

    private void initDefaults() {
        Catalog catalog;
        collection = new HashMap<UUID, Catalog>();

        catalog = new Catalog("Public Catalog", false);
        catalog.setId(UUID.fromString("9cdda1bf-fa8b-4f5f-b0c1-11794fa20717"));
        collection.put(catalog.getId(), catalog);

        catalog = new Catalog("Private Catalog", true);
        catalog.setId(UUID.fromString("eca6cba5-5d9b-4947-833d-bf7add259b7e"));
        collection.put(catalog.getId(), catalog);
    }

    @Override
    public Collection<Catalog> getAll() {
        ArrayList<Catalog> result = new ArrayList<>(collection.values());
        Collections.sort(result, new Comparator<Catalog>() {
            @Override
            public int compare(Catalog o1, Catalog o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
        return Collections.unmodifiableCollection(result);
    }

    @Override
    public Catalog getById(UUID catalogId) {
        System.out.println(collection.toString());
        if (collection.containsKey(catalogId)) {
            System.out.println("2");
            return collection.get(catalogId);
        }
        System.out.println("3");
        return null;
    }

}
