package org.tinylibrary.infrastructure;

import org.tinylibrary.domain.model.Catalog;

import javax.ejb.Local;
import java.util.Collection;
import java.util.UUID;

@Local
public interface ICatalogRepository {
    Collection<Catalog> getAll();
    Catalog getById(UUID catalogId);
}
