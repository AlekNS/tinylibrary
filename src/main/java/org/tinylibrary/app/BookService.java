package org.tinylibrary.app;

import org.tinylibrary.domain.model.Book;
import org.tinylibrary.infrastructure.IBookRepository;
import org.tinylibrary.infrastructure.ICatalogRepository;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.util.Collection;
import java.util.UUID;

@Stateless
@LocalBean
public class BookService {
    @EJB
    IBookRepository bookRepository;

    @EJB
    ICatalogRepository catalogRepository;

    public Collection<Book> getAllBooksByCatalogId(UUID catalogId) {
        return bookRepository.getAllByCatalogId(catalogId);
    }

    public Book getBookById (UUID bookId) {
        Book book = bookRepository.getById(bookId);
        if (book == null) {
            throw new RuntimeException("Book not found");
        }
        return book;
    }

    public Book save(Book book) {
        if (book.getCatalogId() == null) {
            throw new RuntimeException("Book should contains catalog property");
        }
        return bookRepository.save(book);
    }

    public Book removeById(UUID bookId) {
        Book book = bookRepository.getById(bookId);
        if (book == null) {
            throw new RuntimeException("Book not found");
        }
        return bookRepository.remove(book);
    }
}
