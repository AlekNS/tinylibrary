package org.tinylibrary.app;

import org.tinylibrary.domain.model.Catalog;
import java.util.ArrayList;
import java.util.Collection;
import java.util.UUID;

public class CatalogDto {
    private String      id;
    private String      isPrivate;
    private String      name;
    private ArrayList<BookDto> books;

    public CatalogDto() {
        this("", "false");
    }

    public CatalogDto(Catalog catalog) {
        this(catalog.getName(), catalog.getPrivate().toString());
        if (catalog.getId() != null) {
            this.setId(catalog.getId().toString());
        }
    }

    public CatalogDto(String name, String isPrivate) {
        this.name = name;
        this.isPrivate = isPrivate;
        this.books = new ArrayList<>();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPrivate() {
        return isPrivate;
    }

    public void setPrivate(String aPrivate) {
        isPrivate = aPrivate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<BookDto> getBooks() {
        return new ArrayList<>(books);
    }

    public void setBooks(Collection<BookDto> books) {
        this.books = new ArrayList<>(books);
    }

    public Catalog asCatalog() {
        Catalog catalog = new Catalog();
        if (this.getId() != null) {
            catalog.setId(UUID.fromString(this.getId()));
        }
        catalog.setName(this.getName());
        catalog.setPrivate(Boolean.valueOf(this.getPrivate()));
        return catalog;
    }
}
