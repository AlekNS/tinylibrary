package org.tinylibrary.app;

import org.apache.commons.lang3.StringUtils;
import org.tinylibrary.domain.model.Book;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.UUID;

public class BookDto {
    public static final String PUBLISH_DATE_FORMAT = "MM/dd/YYYY";

    private String              id;
    private String              catalogId;
    private String              authors;
    private String              name;
    private String              publishDate;

    public BookDto() {
        this("", "", "", "");
    }

    public BookDto(Book book) {
        this(book.getCatalogId().toString(), StringUtils.join(book.getAuthors(), ","), book.getName(),
                (new SimpleDateFormat(BookDto.PUBLISH_DATE_FORMAT)).format(book.getPublishDate()));
        if (book.getId() != null) {
            this.setId(book.getId().toString());
        }
    }

    public BookDto(String catalogId, String authors, String name, String publishDate) {
        this.catalogId = catalogId;
        this.authors = authors;
        this.name = name;
        this.publishDate = publishDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCatalogId() {
        return catalogId;
    }

    public void setCatalogId(String catalogId) {
        this.catalogId = catalogId;
    }

    public String getAuthors() {
        return this.authors;
    }

    public void setAuthors(String authors) {
        this.authors = authors;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(String publishDate) {
        this.publishDate = publishDate;
    }

    public Book asBook() throws Exception {
        Book book = new Book();
        if (this.getId() != null) {
            book.setId(UUID.fromString(this.getId()));
        }
        book.setName(this.getName());
        if (this.getAuthors() != null) {
            book.setAuthors(Arrays.asList(this.getAuthors().split(",")));
        }
        book.setPublishDate((new SimpleDateFormat(BookDto.PUBLISH_DATE_FORMAT)).parse(this.getPublishDate()));
        book.setCatalogId(UUID.fromString(this.getCatalogId()));

        return book;
    }
}
