
package org.tinylibrary.app;

import org.tinylibrary.domain.model.Catalog;
import org.tinylibrary.infrastructure.ICatalogRepository;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.util.Collection;
import java.util.UUID;

@Stateless
@LocalBean
public class CatalogService {
    @EJB
    ICatalogRepository catalogRepository;

    public Collection<Catalog> getAll() {
        return catalogRepository.getAll();
    }

    public Catalog getCatalogById(UUID catalogId) throws Exception {
        Catalog catalog = catalogRepository.getById(catalogId);
        if (catalog == null) {
            throw new RuntimeException("Catalog not found");
        }
        return catalog;
    }
}
