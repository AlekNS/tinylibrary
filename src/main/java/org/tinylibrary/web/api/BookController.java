package org.tinylibrary.web.api;

import org.tinylibrary.app.BookDto;
import org.tinylibrary.app.BookService;
import javax.ejb.EJB;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.UUID;

@Path("/book")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class BookController {

    @EJB
    BookService bookService;

    public BookController() {
    }


    @GET
    @Path("/{bookId}")
    public Response getBook(@PathParam("bookId") String bookId) {
        try {
            return Response.status(200).entity(new BookDto(bookService.getBookById(UUID.fromString(bookId)))).build();
        } catch (Exception ex) {
            // report to log
            return Response.status(404).build();
        }
    }

    @POST
    @Path("/")
    public BookDto storeBook(@QueryParam("catalogId") String catalogId,
                          BookDto bookDto) throws Exception {
        bookDto.setId(null);
        return saveBook(bookDto, null);
    }



    @PUT
    @Path("/{bookId}")
    public Response updateBook(@PathParam("bookId") String bookId,
                              BookDto bookDto) throws Exception {
        try {
            bookService.getBookById(UUID.fromString(bookId));
            return Response.status(200).entity(this.saveBook(bookDto, bookId)).build();
        } catch (Exception ex) {
            // report to log
        }
        return Response.status(404).build();
    }

    private BookDto saveBook(BookDto bookDto, String bookId) throws Exception {
        if (bookId == null) {
            bookId = UUID.randomUUID().toString();
        }
        bookDto.setId(bookId);

        bookService.save(bookDto.asBook());

        return bookDto;
    }

    @DELETE
    @Path("/{bookId}")
    public Response removeBook(@PathParam("bookId") String bookId) {
        try {
            return Response.status(200).entity(bookService.removeById(UUID.fromString(bookId))).build();
        } catch (Exception ex) {
            // report to log
            return Response.status(404).build();
        }
    }

}
