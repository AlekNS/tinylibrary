package org.tinylibrary.web.api;

import com.google.common.base.Function;
import com.google.common.collect.Lists;
import org.tinylibrary.app.BookDto;
import org.tinylibrary.app.BookService;
import org.tinylibrary.app.CatalogDto;
import org.tinylibrary.app.CatalogService;
import org.tinylibrary.domain.model.Book;
import org.tinylibrary.domain.model.Catalog;

import javax.ejb.EJB;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Collection;
import java.util.UUID;

@Path("/catalog")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class CatalogController {

    @EJB
    CatalogService catalogService;

    @EJB
    BookService bookService;

    @GET
    @Path("/")
    public Collection<CatalogDto> getCatalogs() {
        return Lists.transform(new ArrayList<>(catalogService.getAll()), new Function<Catalog, CatalogDto>() {
            @Override
            public CatalogDto apply(Catalog input) {
            return new CatalogDto(input);
            }
        });
    }

    @GET
    @Path("/{catalogId}")
    public Response getCatalog(@PathParam("catalogId") String catalogId) {
        try {
            Catalog catalog = catalogService.getCatalogById(UUID.fromString(catalogId));
            CatalogDto catalogDto = new CatalogDto(catalog);

            catalogDto.setBooks(Lists.transform(new ArrayList<>(bookService.getAllBooksByCatalogId(catalog.getId())), new Function<Book, BookDto>() {
                @Override
                public BookDto apply(Book book) {
                    return new BookDto(book);
                }
            }));

            return Response.status(200).entity(catalogDto).build();
        } catch (Exception ex) {
            // report to log
            return Response.status(404).build();
        }
    }

}
