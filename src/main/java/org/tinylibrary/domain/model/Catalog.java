package org.tinylibrary.domain.model;

import java.util.UUID;

public class Catalog {
    private UUID        id;
    private Boolean     isPrivate;
    private String      name;

    public Catalog() {
        this("", false);
    }

    public Catalog(String name, Boolean isPrivate) {
        this.name = name;
        this.isPrivate = isPrivate;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Boolean getPrivate() {
        return isPrivate;
    }

    public void setPrivate(Boolean aPrivate) {
        isPrivate = aPrivate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
