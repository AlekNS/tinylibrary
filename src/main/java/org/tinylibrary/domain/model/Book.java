package org.tinylibrary.domain.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.UUID;

public class Book {
    private UUID                id;
    private UUID                catalogId;
    private Collection<String>  authors;
    private String              name;
    private Date                publishDate;

    public Book() {
    }

    public Book(UUID catalogId, Collection<String> authors, String name, Date publishDate) {
        this.catalogId = catalogId;
        this.authors = authors;
        this.name = name;
        this.publishDate = publishDate;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getCatalogId() {
        return catalogId;
    }

    public void setCatalogId(UUID catalogId) {
        this.catalogId = catalogId;
    }

    public Collection<String> getAuthors() {
        return new ArrayList<>(authors);
    }

    public void setAuthors(Collection<String> authors) {
        this.authors = authors;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(Date publishDate) {
        this.publishDate = publishDate;
    }
}
