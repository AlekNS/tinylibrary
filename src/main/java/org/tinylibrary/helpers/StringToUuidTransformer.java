package org.tinylibrary.helpers;

import com.google.common.base.Function;

import java.util.UUID;

public class StringToUuidTransformer implements Function<String, UUID> {
    @Override
    public UUID apply(String input) {
        return UUID.fromString(input);
    }
}
